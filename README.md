# Практическая работа по модулю "Инфраструктура как код (IaC)" курса "DevOps-инженер. Основы"

## Задание №1
Цель задания
* Установить Ansible и использовать его для запуска ReactJS-приложения на сервере, запущенном при помощи Terraform.

Работающее react-приложение на ВМ Yandex Cloud.
![Работающее react-приложение на ВМ Yandex Cloud](docs/img/task1.png)

## Задание №2 (выполните по желанию) 
Цель задания
* Развернуть инфраструктуру с балансировкой нагрузки и двумя серверами при помощи Terraform, установить при помощи Ansible ReactJS приложение одновременно на двух серверах.

Работающее через балансировщик нагрузки react-приложение на первой ВМ Yandex Cloud.
![Работающее через балансировщик нагрузки react-приложение на первой ВМ Yandex Cloud](docs/img/task2-vm-1.png)

Работающее через балансировщик нагрузки react-приложение на второй ВМ Yandex Cloud.
![Работающее через балансировщик нагрузки react-приложение на второй ВМ Yandex Cloud](docs/img/task2-vm-2.png)

## Установка и настройка
1. Скачать дистрибутив Terraform
   * [Установка Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
   * [Дистрибутивы Terrafrom под различные ОС](https://developer.hashicorp.com/terraform/downloads)

1. Распаковать скачанный архив
   ```shell
   unzip <downloaded archive> -d ./terraform
   ```

1. Переместить бинарный файл в `/usr/local/bin/`
   ```shell
   sudo mv ./terraform/terraform /usr/local/bin/
   ```

1. Проверить Terraform
   ```shell
   terraform -help
   ```

1. Удалить лишнее
   ```shell
   sudo rm -f <downloaded archive> && rm -rf terraform
   ```
   
1. [Установить Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

1. [Установить Yandex Cloud CLI](https://cloud.yandex.ru/docs/cli/quickstart#install)

## Работа с Yandex Cloud при помощи Terraform
* [Начало работы с Terraform. Документация Yandex](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)
  
  Обязательно нужно использовать сервисынй аккаунт. Просто через пользовательский аккаунт (user account) у меня не получилось.
  
* [Подключение к ВМ Yandex Cloud по SSH](https://cloud.yandex.ru/docs/compute/operations/vm-connect/ssh#creating-ssh-keys)

* [Yandex.Cloud Provider](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs)

* [Настройки ВМ Yandex.Cloud в Terraform](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance)
  
  По дефолту диск создается очень маленьким - всего 5 Гб. Задания размера диска ищем в настройкe [`size`](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance#size).

* [Работать с Terraform в Яндекс Облаке](https://sidmid.ru/%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D1%82%D1%8C-%D1%81-terraform-%D0%B2-yandex-%D0%BE%D0%B1%D0%BB%D0%B0%D0%BA%D0%B5/)
  
  Хороший пример с пояснениями что, куда и зачем.
  В самом низу пример для балансировщика нагрузки.
  
* [Чтение и использование хостовых переменных окружения в Terraform](https://support.hashicorp.com/hc/en-us/articles/4547786359571-Reading-and-using-environment-variables-in-Terraform-runs)

* [Пример настройки Terraform](https://habr.com/ru/post/684964/)
  
  Хороший пример в трех статьях  с пояснениями что, куда и зачем.
