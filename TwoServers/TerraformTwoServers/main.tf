terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "= 0.82"
    }
  }
  required_version = "= 1.3.4"
}

provider "yandex" {
  zone = "ru-central1-a"
}

data "yandex_compute_image" "ubuntu_image" {
  family = "ubuntu-2204-lts"
}

data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a"  # одна из дефолтных подсетей
}

resource "yandex_compute_instance" "vm-1" {
  name = "module-6-instance-1"

  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat = true
  }

  metadata = {
    # ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("./meta.yml")}"
  }
}

resource "yandex_compute_instance" "vm-2" {
  name = "module-6-instance-2"

  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat = true
  }

  metadata = {
    # ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("./meta.yml")}"
  }
}

resource "yandex_lb_network_load_balancer" "load-balancer" {
  name = "load-balancer"

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }

  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.vm-2.network_interface.0.ip_address
  }
}

output "subnet_id" {
    value = data.yandex_vpc_subnet.default_a.subnet_id
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

output "load_balancer_ip_address" {
  value = yandex_lb_network_load_balancer.load-balancer.*
}
